(function(){
    'use strict';
    angular.module('nopsInChurch', [])
    .controller('nopsController', function($scope, $http, $timeout){
        $scope.urlBASE = "https://reqres.in/api/"
        $scope.totalUsersDeleted = 0;
        $scope.countTimes = 0;
        $scope.editing = false;
        $scope.adding = false;

        $scope.init = function(){
            $scope.requestUsers();
        }

        $scope.returnUsersList = [];
        $scope.requestUsers = function(){
            $scope.countTimes++
            $scope.loading = true;
            $http({
                method: 'GET',
                url: $scope.urlBASE + "users?page=" + $scope.countTimes
            }).then(function sucess(response) {
                $scope.totalUsers = response.data.total;
                $scope.currentPage = response.data.page;
                $scope.userPerPage = response.data.per_page;

                for (let i = 0; i < response.data.data.length; i++) {
                    $scope.returnUsersList.push(response.data.data[i]);
                }
                
                if (($scope.userPerPage * $scope.currentPage) < $scope.totalUsers){
                    $scope.hasMoreUsers = true;
                }else{
                    $scope.hasMoreUsers = false;
                }

                $scope.updateMsg()

                $timeout(function(){ $scope.loading = false; }, 1500);
                

            }, function error(response) {
                alert("Something is wrong. CODReturn = " + response.status);
            })

        }

        $scope.updateMsg = function(){

            if($scope.totalUsersDeleted == 0){
                $scope.totalUsers = $scope.totalUsers;
            }else{
                $scope.totalUsers = ($scope.totalUsers - $scope.totalUsersDeleted)
            }
            
            $scope.message = "Showing " + $scope.returnUsersList.length + " of " + $scope.totalUsers;
        }

        $scope.edit = function(event){
            $scope.editing = ($scope.editing)? false : true;
            $scope.elementParentNodeId = event.target.parentNode.id;

            if($scope.editing){
                $("#" + $scope.elementParentNodeId + " .cancelBtn").show()
                $("#" + $scope.elementParentNodeId + " .updateBtn").show()
                $("#" + $scope.elementParentNodeId + " .deleteBtn").show()

                $("#" + $scope.elementParentNodeId + " #firstName").prop( "disabled", false );
                $("#" + $scope.elementParentNodeId + " #lastName").prop( "disabled", false );

            }else{
                $("#" + $scope.elementParentNodeId + " .cancelBtn").hide()
                $("#" + $scope.elementParentNodeId + " .updateBtn").hide()
                $("#" + $scope.elementParentNodeId + " .deleteBtn").hide()

                $("#" + $scope.elementParentNodeId + " #firstName").prop( "disabled", true );
                $("#" + $scope.elementParentNodeId + " #lastName").prop( "disabled", true );
            }
        }

        $scope.checkForSubmitUpdates = function(event){
            $scope.submitUpdates(event);
        }

        $scope.submitUpdates = function(event){

            $scope.userId = event.target.parentNode.id;
            $scope.elementParentNodeId =  event.target.parentNode.id;
            $scope.userFirstName = $("#" + $scope.elementParentNodeId + " #firstName")[0].value;
            $scope.userLastName = $("#" + $scope.elementParentNodeId + " #lastName")[0].value;

            var data = JSON.parse('{ "first_name": "'+ $scope.userFirstName + '", "last_name": "'+ $scope.userLastName +'"}'); 
            $http({
                method: 'PUT',
                url: $scope.urlBASE + "users/" + $scope.userId,
                data: data
            }).then(function sucess(response) {
                $scope.edit(event)
                alert("The user was updated to " + response.data.first_name + " " + response.data.last_name + " at: " + new Date(response.data.updatedAt));
            },function error(response){
                alert("Something is wrong. CODReturn = " + response.status);
            })
        }

        $scope.confirmForDelete = function(event) {
            if(confirm("Are you sure ? It'll remove this user from the list, forever !")){
                $scope.deleteUser(event);
            }
        }
        $scope.deleteUser = function (event) {
            $scope.userId = Number(event.target.parentNode.id);
            $http({
                method: 'DELETE',
                url: $scope.urlBASE + "users/" + $scope.userId
            }).then(function sucess(response) {

                for( var i = 0; i < $scope.returnUsersList.length; i++){ 
                    if ( $scope.returnUsersList[i].id === ($scope.userId)) {
                        $scope.returnUsersList.splice(i, 1);
                    }
                 }
                $scope.totalUsersDeleted++
                $scope.updateMsg()
                $scope.totalUsersDeleted = 0
                $scope.editing = ($scope.editing)? false : true;
                alert("This user was deleted. ");
            },function error(response){
                alert("Something is wrong. CODReturn = " + response.status);
            })
        }


        $scope.addNewUser = function () {
            if($scope.adding){
                $scope.adding = false;
            }else{
                $scope.adding = true;
            }
        }

        $scope.checkForSubmitNewUser = function(){
            if($scope.newLastName == ""){
                $(".newLastName")[0].focusin()
            }else if($scope.newFirstName == ""){
                $(".newFirstName")[0].focusin()
            }else if($scope.newUrlLink == ""){
                $(".newUrlLink")[0].focusin()
            }else{
                $scope.submitNewUser()
            } 
        }

        $scope.submitNewUser = function () {
            $scope.loading = true;
            $scope.newUserID = ($scope.totalUsers + 1);
            
            var data = JSON.parse('{ "first_name": "'+ $scope.newFirstName + '", "last_name": "'+ $scope.newLastName +'", "avatar": "'+ $scope.newUrlLink  +'", "id": ' + $scope.newUserID + '}'); 
            $http({
                method: 'POST',
                url: $scope.urlBASE + "users/",
                data: data
            }).then(function sucess(response) {
                $scope.returnUsersList.push(data);
                alert("The user " + $scope.newFirstName + " " + $scope.newLastName + " was created with id " + $scope.newUserID);
                

                $scope.totalUsers++
                $scope.updateMsg()
                $scope.newLastName = ""
                $scope.newFirstName = ""
                $scope.newUrlLink = ""
                $scope.newUserID = ""
                $timeout(function(){ $scope.loading = false; }, 1500);

                $scope.addNewUser()

            }, function error(response){
                alert("Something is wrong. CODReturn = " + response.status);
            });
        }
    })

})()